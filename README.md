# Dessiner une rue avec [turtle.py](https://docs.python.org/fr/3/library/turtle.html)

![C BO](image.png)

Par _Louis BRUNETEAU_

Ce module permet de dessiner des immeubles dans une rue en utilisant le module [turtle.py](https://docs.python.org/fr/3/library/turtle.html) aussi simplement que possible. Il a pour but la possibilité de créer le quartier que l'on désire en quelques lignes.

## Exemples d'utilisation

```python
import rue
rue.immeuble(5, 'blue', rue.COORD_DEPART)
```

![C BO](./exemple.png)

## Dépendances

- [*Python 3*](https://www.python.org/)
- *Tkinter*
- Turtle
Tous trois devraient être préinstallés.

### Importer le module

Pour importer le module vous pouvez **lancer le fichier** depuis un terminal avec l'option `-i`

```zsh
python3 -i rue.py
````

Vous pouvez également l\'**importer** depuis python.

```python
import rue
```

ou

```python
from rue import *
```

### Utiliser le module

Le module contient un certain nombre de **constantes** utilisées dans la création d'immeubles.

#### Constantes de taille / coodonnées / dimensions

| Nom de la constante | Type | Valeur par défaut | Description |
|:-------------------:|:----:|:-----------------:|-------------|
| `ETAGES_MIN` | `int` | `2` | Nombre d'étages minimal pour un immeuble |
| `ETAGES_MAX` | `int` | `5` | Nombre d'étages maximal pour un immeuble |
| `TAILLE_IMMEUBLE` | `int` | `140` | La largeur d'un immeuble en pixels |
| `TAILLE_ETAGE` | `int` | `60` | La hauteur d'un étage |
| `TAILLE_FENETRE` | `Tuple[int, int]` | `(30, 30)` | Les dimensions d'une fenêtre |
| `TAILLE_PORTE` | `Tuple[int, int]` | `(30, 45)` | Les dimensions d'une porte |
| `TAILLE_BALCON` | `Tuple[int, int]` | `(30, 50)` | Les dimensions d'un balcon |
| `TAILLE_PORTE_FENETRE` | `Tuple[int, int]` | `(30, 30)` | Les dimensions du porte fenêtre |
| `NB_BARREAUX` | `int` | `6` | Le nombre de barreaux par porte fenêtre |
| `X_FENETRES` | `Tuple[int, int, int]` | `(12, 54, 96)` | Les coordonnées x de la position des fenêtes sur les façades |
| `BASE_TOIT` | `Tuple[float, float]` | `(160, 25)` | La taille du rectangle constituant la base du toît |
| `HAUTEUR_TOIT` | `float` | `45` | La hauteur du sommet du toît par rapport à sa base
| `PROBA_BALCON` | `int` | `4` | Une chance sur `PROBA_BALCON` qu'une fenêtre soit un balcon |
| `COORD_DEPART` | `Tuple[float, float]` | `(-450, -380)` | Les coordonnées de la tortue au début de l'animation |
| `IMMEUBLES_MIN` | `int` | `3` | Le nombre minimal d'immeubles |
| `IMMEUBLES_MAX` | `int` | `5` | Le nombre maximal d'immeubles |

#### Constantes de couleur

Une couleur est une chaîne de couleur *Tk* ou un tuple *RGB*.
Les ensembles de couleur sont sous la forme de *set* de couleurs.

```python
COULEUR = Union[str, Tuple[int, int, int]]
```

- `COULEUR_FENETRE` : `#7CFFFF`
- `COULEUR_PORTE` : `brown`, `grey`
- `COULEUR_FACADE` : `red`, `green`, `yellow`, `pink`, `blue`, `purple`, `orange`
- `COULEUR_TOIT`: `black`

### Fonctions

#### `aller`

La fonction `aller` lève le crayon, va aux coordonnées indiquées, et baisse le crayon

|Argument|Explication|
|---|---|
| `coord (Tuple[float, float])`| Les coordonnées|

#### `rect`

La fonction `rect` dessine un rectangle de taille, de couleur et de position donnée

|Argument|Explication|
|---|---|
| `taille (Tuple[float, float])`|Les dimensions du rectangle|
| `couleur (COULEUR)` | La couleur de remplissage|
| `pos (Tuple[float, float], optional)` | Coordonnées du coin inférieur droit. `turtle.pos()` par défaut|

#### `porte`

Dessine une porte aux coordonneés et de la couleur données

|Argument|Explication|
|---|---|
coord `(Tuple[float, float], optional)` | Coordonnées de la porte. `turtle.pos()` par défaut
`couleur (COULEUR, optional)` | Couleur de la porte. Aléatoire parmi les couleurs de porte par défaut

#### `fenetre`

Dessine une fenetre aux coordonnées voulues

|Argument|Explication|
|---|---|
`coord (Tuple[float, float], optional)` | Coordonneeés de la fenetre. `turtle.pos()` par défaut

#### `balcon`

Dessine un balcon ainsi que des barreaux aux coordonnées indiquées.

|Argument|Explication|
|---|---|
`coord (Tuple[float, float], optional)` | Coordonneeés du balcon. `turtle.pos()` par défaut

#### `toit`

Dessine le toit d'un immeuble aux coordonnées

|Argument|Explication|
|---|---|
`coord (Tuple[float, float], optional)` | l'angle supérieur gauche de l'immeuble. `turtle.pos()` par défaut

#### `immeuble`

Dessine un immeuble

|Argument|Explication|
|---|---|
`etages (int)` | Le nombre d'étages de l'immeuble
`color (COULEUR)`| La couleur de la façade
`coord (Tuple[float, float], optional)` | Coordonneeés de l'immeuble. `turtle.pos()` par défaut

#### `main`

Dessine une rue aléatoire.
