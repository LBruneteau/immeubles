'''
Tp immeubles
'''
from turtle import Turtle, done
import random
from typing import Tuple, List, Union, Set, Optional

turtle: Turtle = Turtle()

# Constantes sur les dimensions / Coordonnées
ETAGES_MIN: int = 2
ETAGES_MAX: int = 5
TAILLE_IMMEUBLE: int = 140
TAILLE_ETAGE: int = 60
TAILLE_FENETRE: Tuple[int, int] = (30, 30)
TAILLE_PORTE: Tuple[int, int] = (30, 45)
TAILLE_BALCON: Tuple[int, int] = (30, 50)
TAILLE_PORTE_FENETRE: Tuple[int, int] = (30, 30)
NB_BARREAUX: int = 6
X_FENETRES: Tuple[int, int, int] = (12, 54, 96)
BASE_TOIT: Tuple[float, float] = (160, 25)
HAUTEUR_TOIT: float = 45
PROBA_BALCON: int = 4 # 1 chance / prob
COORD_DEPART: Tuple[float, float] = (-450, -380)
IMMEUBLES_MIN: int = 3
IMMEUBLES_MAX: int = 5

# Constantes de couleurs
# Une couleur est une chaîne de couleur Tk ou un tuple RGB
COULEUR = Union[str, Tuple[int, int, int]]
COUELUR_FENETRE: COULEUR = '#7CFFFF'
COULEUR_PORTE: Set[COULEUR] = {'brown', 'grey'}
COULEUR_FACADE: Set[COULEUR] = {
    'red', 'green', 'yellow', 'pink',
    'blue', 'purple', 'orange'}
COULEUR_TOIT: COULEUR = 'black'

def aller(coord: Tuple[float, float]) -> None:
    '''Lève le crayon pour aller aux coordonnées et le repose

    Args:
        coord (Tuple[int, int]): Les coordonnées
    '''
    turtle.up()
    turtle.goto(coord)
    turtle.down()

def rect(taille: Tuple[float, float], couleur: COULEUR, pos: Tuple[float, float] = turtle.pos()) -> None:
    '''Dessine un rectangle avec turtle.py

    Args:
        taille (Tuple[int, int]): Les dimensions du rectangle
        couleur (COULEUR): La couleur de remplissage
        pos (Tuple[int, int], optional): Coordonnées du coin inférieur droit.
    '''
    aller(pos)
    turtle.setheading(0)
    turtle.color('black', couleur)
    turtle.begin_fill()
    for cote in range(4):
        turtle.forward(taille[cote % 2])
        turtle.left(90)
    turtle.end_fill()


def porte(coord: Tuple[float, float] = turtle.pos(), couleur: Optional[COULEUR] = '') -> None:
    '''Dessine une porte aux coordonneés et de la couleur

    Args:
        coord (Tuple[int, int], optional): Coordonnées de la porte. Defaults to turtle.pos().
        couleur (COULEUR, optional): Couleur de la porte. Aléatoire parmi les couleurs de porte par défaut
    '''
    if not couleur:
        couleur = random.sample(COULEUR_PORTE, 1)
    rect(TAILLE_PORTE, couleur, coord)


def fenetre(coord: Tuple[float, float] = turtle.pos()) -> None:
    '''Dessine une fenetre

    Args:
        coord (Tuple[int, int], optional): Coordonneeés de la fenetre. Defaults to turtle.pos().
    '''
    rect(TAILLE_FENETRE, COUELUR_FENETRE, coord)


def balcon(coord: Tuple[float, float] = turtle.pos()) -> None:
    '''Dessine un balcon

    Args:
        coord (Tuple[float, float], optional): [description]. Defaults to turtle.pos().
    '''
    rect(TAILLE_BALCON, COUELUR_FENETRE, coord)
    turtle.pensize(3)
    rect(TAILLE_PORTE_FENETRE, COUELUR_FENETRE, coord)
    turtle.left(90)
    for _ in range(NB_BARREAUX):
        x, y = turtle.pos()
        turtle.forward(30)
        # Savoir l'emplacement en fonction du nombre de barreaux
        aller((x + TAILLE_BALCON[0] // NB_BARREAUX, y))
    turtle.pensize(1)


def toit(coord: Tuple[float, float] = turtle.pos()) -> None:
    '''Dessine le toit de l'immeuble

    Args:
        coord (Tuple[float, float]): Le coin supérieur gauche de l'immeuble
    '''
    rect(BASE_TOIT, 'black', (coord[0] - 10, coord[1]))
    turtle.left(90)
    turtle.forward(BASE_TOIT[1])
    turtle.color('black', COULEUR_TOIT)
    turtle.begin_fill()
    for _ in range(2):
        turtle.right(60)
        turtle.forward((HAUTEUR_TOIT ** 2 + (BASE_TOIT[0] / 2) ** 2) ** 0.5)
    turtle.end_fill()


def immeuble(etages: int, color: COULEUR, coord: Tuple[float, float] = turtle.pos()) -> None:
    '''Dessine un immeuble.

    Args:
        etages (int): nombre d'étages de l'immeuble
        color (str): Couleur de remplissage
    '''
    aller(coord)
    x, y = turtle.pos()
    rect((TAILLE_IMMEUBLE, TAILLE_ETAGE * etages), color, (x, y))
    possibilites_porte: List[int] = list(X_FENETRES)
    random.shuffle(possibilites_porte)
    porte((x + possibilites_porte.pop(), y))
    for fen in possibilites_porte:
        # Là où il n'y a pas de portes on met des fenetres
        fenetre((x + fen, y + 20))
    for etage in range(etages - 1):
        for fen in X_FENETRES:
            turtle.up()
            if random.randrange(PROBA_BALCON):
                fenetre((x + fen, y + TAILLE_ETAGE * (etage + 1) + 20))
            else:
                balcon((x + fen, y + TAILLE_ETAGE * (etage + 1)))
            turtle.up()
            turtle.forward(42)
    toit((x, y + TAILLE_ETAGE * etages))
    aller((x, y))

def main():
    '''Dessine une rue d'immeubles aléatoirement
    '''
    colors: List[str] = list(COULEUR_FACADE)
    random.shuffle(colors)
    turtle.speed(100)
    aller(COORD_DEPART)
    for _ in range(random.randint(IMMEUBLES_MIN, IMMEUBLES_MAX)):
        immeuble(random.randint(ETAGES_MIN, ETAGES_MAX), colors.pop(), turtle.pos())
        turtle.up()
        turtle.setheading(0)
        turtle.forward(180)
    turtle.hideturtle()
    done()


if __name__ == "__main__":
    main()
